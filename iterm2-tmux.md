# tmux



**滚动缓冲区**

```
设置滚动的正确方法：在 tmux.conf
set -g mouse on        #For tmux version 2.1 and up
要么
set -g mode-mouse on   #For tmux versions < 2.1
```

https://qastack.cn/superuser/209437/how-do-i-scroll-in-tmux



- 复制

https://codeday.me/bug/20170816/56414.html









# vim





- 浏览代码

https://coolshell.cn/articles/11312.html



- 缓冲区

https://coolshell.cn/articles/11312.html



**ubuntu 16**

- upgrade  vim 8

https://itsfoss.com/vim-8-release-install/



- 配色方案

https://github.com/altercation/vim-colors-solarized

https://www.kawabangga.com/posts/1887

https://havee.me/linux/2013-10/vim-colorscheme.html



**插件**

- AutoPairs

https://github.com/jiangmiao/auto-pairs

- Surround

https://github.com/tpope/vim-surround













# zsh









-------

## tmuxinator

[**tmuxinator**](https://github.com/Tmuxinator/Tmuxinator)

https://wiki.tankywoo.com/tool/tmux.html

http://ryan.himmelwright.net/post/setting-up-tmuxinator/

https://levonfly.github.io/p/29f1e79c.html

https://zhuanlan.zhihu.com/p/71235052

